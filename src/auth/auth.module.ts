import { HttpModule, Module, HttpService } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './jwtConfig';
import { UserService } from '../user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../user/user.repository';
import { JwtStrategy } from './jwt.strategy';
import { User } from '../user/user.entity';
import { AuthController } from './auth.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository]),
      UserModule,HttpModule, PassportModule,JwtModule.register({
      secret:jwtConstants.secret,
      signOptions:{expiresIn:jwtConstants.expiration}
  })],
  controllers:[AuthController],
  providers: [AuthService,LocalStrategy,UserService,JwtStrategy],
  exports: [AuthService,JwtModule],
})
export class AuthModule {}