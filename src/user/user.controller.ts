import { Controller, Get, Body, Post, Param, Delete, Patch, UseGuards, Res, Req, ValidationPipe, UsePipes, Request } from '@nestjs/common';
import { AuthGuard } from '../user.guard';
import { UserService } from './user.service';
import { SignupDto } from './dto/signup.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { SigninDto } from './dto/signin.dto';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post("register")
  @UsePipes(ValidationPipe)
  async register(@Body() signup:SignupDto){
    console.log(signup);
    let resp = await this.userService.create(signup);
    return {message:resp.message,user:{username:resp.value["username"],name:resp.value["name"]}};
  }
}