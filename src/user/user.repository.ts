import { genSalt, hash } from "bcryptjs";
import { EntityRepository,  Repository } from "typeorm";
import { SignupDto } from "./dto";
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {


  async signup(signupDto: SignupDto)  {
    const { username, password } = signupDto;
    const user = new User();
    user.username = username;
    user.name = signupDto.name;
    const salt = await genSalt(10);
    user.password = await hash(password, salt);

    try {
      await user.save();
      let data = {
        message: 'Usuario creado',
        status: true,
        value:user,
        userCreated:true
      }
      return data;
    } catch (error) {
      console.log(error)
      const data = {
        message: "error al crear usuario",
        status: true,
        value:user,
        userCreated:false
      }
      return data;
    }

  }
  async test(username:String):Promise<User>{
      return super.findOne({where:{username:username}});
  }
}
