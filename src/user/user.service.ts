import {
    Injectable, HttpService
  } from '@nestjs/common';
  import { UserRepository } from './user.repository';
  import { InjectRepository } from '@nestjs/typeorm';
  import { User } from './User.entity';
import { SignupDto } from './dto/signup.dto';
import { Repository } from 'typeorm';

  
  @Injectable()
  export class UserService {
  
    constructor(
      @InjectRepository(UserRepository)
      private readonly userRepository: UserRepository,
      private _http: HttpService
    ) { }
  
    async findOne(username:String):Promise<User>{
        return this.userRepository.findOne({where:{username:username}});
    }

    async create(user:SignupDto):Promise<any>{
        return await this.userRepository.signup(user);
    }
    
  }