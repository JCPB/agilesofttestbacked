import { BaseEntity,Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Todo extends BaseEntity{
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 50 })
  state: string;

  @Column({ length: 100 })
  descript: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
  creationDate: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
  lastModificatione: Date;

  @Column("int", { name: "USUARIO_ID", nullable: true })
  usuarioId: number | null;

}