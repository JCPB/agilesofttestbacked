import { IsDateString, IsNotEmpty, IsString } from 'class-validator';
export class TodoDto {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    state: string;

    @IsNotEmpty()
    @IsString()
    descript: string;

    @IsDateString()
    creationDate: Date|null ;

    @IsDateString()
    lastModificatione: Date|null ;
  }