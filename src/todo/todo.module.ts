import { Module } from '@nestjs/common';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import { AuthModule } from '../auth/auth.module';
import { Todo } from './todo.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports:[AuthModule,TypeOrmModule.forFeature([
    Todo
  ])],
  controllers: [TodoController],
  providers: [TodoService],
})
export class TodoModule {}