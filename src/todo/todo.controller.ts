import { Controller, Get, Body, Post, Param, Delete, Patch, UseGuards, Res, Req, ValidationPipe, UsePipes } from '@nestjs/common';
import { TodoService } from './todo.service';
import { AuthGuard } from '../user.guard';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { TodoDto } from './todo.dto';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  getAllTodos(@Req() req): any {
    let user = req.user;
    return this.todoService.getAll(user.id)
  }

  @Post()
  @UsePipes(ValidationPipe)
  addTodo(@Req() req,@Body() todo: TodoDto): any {
    return this.todoService.createTodo(req.user.id,todo);
  }

  @Get(':id')
  getTodoById(@Req() req,@Param('id') todoId:number): any {
    return this.todoService.get(req.user.id,todoId);
  }

  @Delete(':id')
  deleteTodoById(@Req()req,@Param('id') todoId: number): any {
    return this.todoService.deleteById(req.user.id,todoId);
  }

  @Patch(':id')
  @UsePipes(ValidationPipe)
  updateTodoById(
    @Req()req,
    @Param('id') todoId: number,
    @Body() todoUpdate:TodoDto): any {
    return this.todoService.UpdateById(req.user.id,todoId, todoUpdate);
  }
}