import { Injectable, NotFoundException } from '@nestjs/common';
import { TodoDto } from './todo.dto';
import { generate } from 'shortid';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Todo } from './todo.entity';
@Injectable()
export class TodoService {
  
    constructor(
        @InjectRepository(Todo)
        private readonly todoRepository:Repository<Todo>,
    ){}

    async getAll(userId:number):Promise<Todo[]>{
        console.log("trying to push");
        return await this.todoRepository.find({where:{usuarioId:userId}});
    }
    async createTodo(userId:number,newTodo:TodoDto):Promise<Todo>{
        const nuevo = new Todo();
        for (const key of Object.keys(newTodo)) {
            nuevo[key]=newTodo[key];
        }
        nuevo.usuarioId=userId;

        return nuevo.save().then((e)=>{
                return this.todoRepository.findOne(e.id);
        });
    }

    async get(userId:number,todoId:number):Promise<Todo[]>{
        return await this.todoRepository.find({where:{usuarioId:userId,id:todoId}});
        }

    async deleteById(userId:number,todoId: number): Promise<any>{
        let findTodo = await this.todoRepository.findOne(todoId);
        if (findTodo.usuarioId!=userId) {
        throw new NotFoundException();
        }else{
            this.todoRepository.remove(findTodo);
        }
        return { message: 'Todo Deleted' };
    }
    async UpdateById(userId:number,todoId:number,todoUpdate:TodoDto):Promise<Todo> {
        let findTodo = await this.todoRepository.findOne({where:{usuarioId:userId,id:todoId}});
        if (findTodo.usuarioId!=userId) {
        throw new NotFoundException();
        }else{
            for (const key of Object.keys(todoUpdate)) {
                findTodo[key]=todoUpdate[key];
            }
            return findTodo.save().then(()=>{
                return this.todoRepository.findOne(todoId);
            })
        }
    }
}